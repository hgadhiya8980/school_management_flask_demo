# School Management Demo

# Live Demo
https://flaskdemo-j0j4.onrender.com/

# Code Repo
https://gitlab.com/hgadhiya8980/school_management_flask_demo

# Functionality Include
Login
Register
Mail Integration
Admin Panel
2FA (Mail OTP Authentication)
Show Student Data in Admin Panel


# Admin Panel Credential
username: admin
password: admin

# Student Panel
Step:1 > Please do register with your correct details because we need to authenticate

Step:2 > Login with your latest credential